core = 7.x
api = 2

; services
projects[services][type] = "module"
projects[services][download][type] = "git"
projects[services][download][url] = "https://git.uwaterloo.ca/drupal-org/services_.git"
projects[services][download][tag] = "7.x-3.6"

; services_api_key_auth
projects[services_api_key_auth][type] = "module"
projects[services_api_key_auth][download][type] = "git"
projects[services_api_key_auth][download][url] = "https://git.uwaterloo.ca/drupal-org/services_api_key_auth.git"
projects[services_api_key_auth][download][tag] = "7.x-1.0"

; services_views
projects[services_views][type] = "module"
projects[services_views][download][type] = "git"
projects[services_views][download][url] = "https://git.uwaterloo.ca/drupal-org/services_views.git"
projects[services_views][download][tag] = "7.x-1.0+4-dev-uw_wcms2"

; uw_ct_food_outlet
projects[uw_ct_food_outlet][type] = "module"
projects[uw_ct_food_outlet][download][type] = "git"
projects[uw_ct_food_outlet][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_food_outlet.git"
projects[uw_ct_food_outlet][download][tag] = "7.x-1.10"

; uw_food_services
projects[uw_food_services][type] = "module"
projects[uw_food_services][download][type] = "git"
projects[uw_food_services][download][url] = "https://git.uwaterloo.ca/wcms/uw_food_services.git"
projects[uw_food_services][download][tag] = "7.x-0.7"

; office_hours
projects[office_hours][type] = "module"
projects[office_hours][download][type] = "git"
projects[office_hours][download][url] = "https://git.uwaterloo.ca/drupal-org/office_hours.git"
projects[office_hours][download][tag] = "7.x-1.3"
